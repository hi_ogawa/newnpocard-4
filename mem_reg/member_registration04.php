<?php		// member_registration04.php

 //
	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');	
	charSetUTF8();
	session_start();
//接続
 	try {
    // MySQLサーバへ接続
   	$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e){
    	die($e->getMessage());
	}

	$stmt = $pdo->prepare("SELECT * FROM `dr_tbl` WHERE `email` = :email AND `is_usable` = :is_usable;");
	$stmt->bindValue(":email", $_GET['email']);
	$stmt->bindValue(":is_usable", "1");
	$stmt->execute();
	$users = $stmt->fetch(PDO::FETCH_ASSOC);
	if (($stmt->rowCount()>0)&&(substr(hash("sha512", $magic_code.$_GET['email']), 10, 32) == $_GET['md5'])) {

		$stmt = $pdo->prepare("UPDATE `dr_tbl` SET `is_active` = :is_active WHERE `email` = :email AND `is_usable` = :is_usable;");
		$stmt->bindValue(":is_active", "1");
		$stmt->bindValue(":email", $_GET['email']);
		$stmt->bindValue(":is_usable", "1");
		$stmt->execute();
 		$message="Your email address was successfully authenticated. <br />".
			"<a href='../index.php'>Please go to login.</a><br /><br />".
			"あなたの電子メール・アドレスは正しく認証されました。<br /><a href='../index.php'>どうぞログインに進んで下さい。</a>";
		
		//　研究者登録があったことをメールで知らせる
		$body = $users['dr_name']."[".$users['sirname']."  ".$users['firstname']."]"."(".$users['hp_name'].") 先生が特定非営利活動法人ティー・アール・アイ国際ネットワークに登録されました";
		$subject = "新規が登録ありました";
		$sender = mb_encode_mimeheader("特定非営利活動法人ティー・アール・アイ国際ネットワーク参加登録");
		$headers  = "FROM: ".$sender."<$support_mail>\r\n";	
		$parameters = '-f'.$support_mail;
		

		mb_language("uni"); //Unicode（UTf-8）でメール送信するための宣言
		mb_send_mail('transradial@kamakuraheart.org, rie_iwamoto@kamakuraheart.org, kaori_sudo@kamakuraheart.org', $subject, $body, $headers, "-f$support_mail");

		$_SESSION = array();
		session_destroy();
 	} else {
 		$message="Your email address was not authenticated<br />Contact to {$support_mail}<br /><br />".
			"その電子メール・アドレスの認証に失敗しました。<br />理由については{$support_mail}までお問い合わせ下さい。";
 	}
	
?>

<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<head>
	<script src="../javascript/jquery-1.10.2.js"></script>
	<title>ID Registration [4/4]</title>
</head>
<body>
<div align="center">
<h1>ID Registration [4/4]</h1><br/><br/>
<?=$message?>
</div>
</body>
</html>

