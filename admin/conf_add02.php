<?php		// conf_add02.php

	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');	
	charSetUTF8();
	session_start();
	$today_year = date("Y");
	if ($_SESSION['conf_add'] != hash("sha512", $today_year)) header('Location: conf_add01.php');

	if (!isset($_SESSION['auth_admin'])||($_SESSION['auth_admin'] != hash("sha512", $magic_code.'facc'))) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}

 	//入力エラーのチェック
	$error='';
	if ($_POST['conf_jname']=='') $error.='カンファランス名がありません<br />';
		else $_SESSION['conf_jname'] = $_POST['conf_jname'];
	if (!mb_check_encoding($_POST['conf_jname'], 'UTF-8')) $error.='カンファランス名が不正な文字列です<br />';
	if (strlen($_POST['conf_jname'])>64) $error.='カンファランス名が長すぎます<br />';
	if ($_POST['conf_ename']=='') $error.='Conference nameがありません<br />';
		else $_SESSION['conf_ename'] = $_POST['conf_ename'];
	if (!mb_check_encoding($_POST['conf_ename'], 'UTF-8')) $error.='Conference nameが不正な文字列です<br />';
	if (!preg_match("/^[0-9a-zA-Z\s]+$/",$_POST['conf_ename'])) $error.='Conference nameに半角アルファベット以外の文字が使われています<br />';
	if (strlen($_POST['conf_ename'])>128) $error.='Conference nameが長すぎます<br />';
	
	if ($_POST['jplace']=='') $error.='場所がありません<br />';
		else $_SESSION['jplace'] = $_POST['jplace'];
	if (!mb_check_encoding($_POST['jplace'], 'UTF-8')) $error.='場所が不正な文字列です<br />';
	if (strlen($_POST['jplace'])>64) $error.='場所が長すぎます<br />';
	if ($_POST['eplace']=='') $error.='Venueがありません<br />';
		else $_SESSION['eplace'] = $_POST['eplace'];
	if (!mb_check_encoding($_POST['eplace'], 'UTF-8')) $error.='Venueが不正な文字列です<br />';
	if (!preg_match("/^[a-zA-Z\s\(\)\[\]\,]+$/",$_POST['eplace'])) $error.='Venueに半角アルファベット以外の文字が使われています<br />';
	if (strlen($_POST['eplace'])>128) $error.='Venueが長すぎます<br />';

	if (!is_numeric($_POST['man_size'])||($_POST['man_size']<0)||($_POST['man_size']>5000)) $error.='不正な人数です<br />';
		else $_SESSION['man_size'] = $_POST['man_size'];
		
	if (($_POST['begin'] == '')||($_POST['end'] == '')) $error.='日付入力がありません<br />';
	if (!preg_match('/2[0-9]{3}-[01][0-9]-[0-3][0-9]/', $_POST['begin'])) $error.='開始日が正しくありません<br />';
		else {
			list($year, $mon, $day) = explode('-', $_POST['begin']);
			if (!checkdate($mon, $day, $year))  $error.='開始日が正しくありません<br />';
		}
	if (!preg_match('/2[0-9]{3}-[01][0-9]-[0-3][0-9]/', $_POST['end'])) $error.='終了日が正しくありません<br />';
		else {
			list($year, $mon, $day) = explode('-', $_POST['end']);
			if (!checkdate($mon, $day, $year))  $error.='終了日が正しくありません<br />';
		}	

	//エラーのある場合
	if ($error != '') {
?>

<html><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xlmns="http://www.w3.org/1993/xhtml" xml:lang="ja" lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="member_registration.css">
 	<title>参加登録画面 [2/4]</title>
</head>
<body>
	<div align="center">
	<h1>参加登録画面 [2/4]</h1><br/><br/>
	<p id="error"><?=$error ?></p><br />	
	<h3>ブラウザの「戻る」ボタンを用いて戻り、間違いを訂正して下さい</h3>
	</div>
</body>

</html>
<?php
 	exit();
 }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="member_registration.css"/>
<title>参加登録画面 [2/4]</title>
</head>
<body bgcolor="#AADDFF">
<div align="center">
<h1>カンファランス登録画面<br/><br/></h1>

<table>
<form action="conf_add03.php" method="post">
	<input type="hidden" name="conf_jname" value="<?= _Q($_POST['conf_jname']) ?>" />
	<input type="hidden" name="conf_ename" value="<?= _Q($_POST['conf_ename']) ?>" />
    <input type="hidden" name="jplace" value="<?=_Q($_POST['jplace']) ?>" />
    <input type="hidden" name="eplace" value="<?= _Q($_POST['eplace']) ?>" />
    <input type="hidden" name="begin" value="<?= _Q($_POST['begin']) ?>" />
    <input type="hidden" name="end" value="<?= _Q($_POST['end']) ?>" />   
    <input type="hidden" name="man_size" value="<?= $_POST['man_size'] ?>" />

    <tr><td>カンファランス名 : </td>
    <td><?= _Q($_POST['conf_jname']) ?></td></tr>
    <tr><td>カンファランス名(半角アルファベット) : </td>
    <td><?= _Q($_POST['conf_ename']) ?></td></tr> 
    <tr><td>開始日(YYYY-MM-DD) : </td>
    <td><?= _Q($_POST['begin']) ?></td></tr>     
    <tr><td>終了日(YYYY-MM-DD) : </td>
    <td><?= _Q($_POST['end']) ?></td></tr> 
    <tr><td>場所 : </td>
    <td><?= _Q($_POST['jplace']) ?></td></tr> 
    <tr><td>Venue : </td>
    <td><?= _Q($_POST['eplace']) ?></td></tr> 
    <tr><td>人数 : </td>
    <td><?= _Q($_POST['man_size']) ?></td></tr> 

<tr><td colspan="2" align="center"><input type="submit" value="- 確認 -"></td></tr>
</form>
</div>

</body>
</html>