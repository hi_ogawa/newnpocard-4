<?php
	//gmo_mail.php
	//**************************************************************
	//	GMO決済後、完了通知メール送信クラス
	//	
	//	
	//**************************************************************

	readfile("http://".$_SERVER["HTTP_HOST"]."/members/utilities/config.php");
	readfile("http://".$_SERVER["HTTP_HOST"]."/members/utilities/lib.php");

	class gmo_mail {

		public function send2admin($dr_name,$email,$hp_name,$job_kind)
		{
			global $support_mail;
			global $site_name;
			global $support_mail;
			global $sender;
			global $job_kinds;
			global $site_url;

			mb_language("Japanese");
			mb_internal_encoding("UTF-8");
			mb_http_output('UTF-8');

			$subject = "${site_name} メール送信エラー通知メール";
			$sender = mb_encode_mimeheader("NPO TRI International Network");
			$headers  = "FROM: ".$sender."<$support_mail>\r\n";		

	//　ここからメール内容
	$body = <<< _EOT_
Dear Mr/Ms/Dr {$dr_name}:
  Email: {$email}
  Affiliation: {$hp_name};
  Speciality: {$job_kinds[$job_kind]};
	
メール送信エラー

ご確認お願いいたします。

===========================================================
	$site_name
	$site_url
_EOT_;
	// _EOT_　は行の最初に記述し、;の後にコメントも無し、にしないとエラーとなる	
		
		$sendto = 'transradial@kamakuraheart.org, rie_iwamoto@kamakuraheart.org, kaori_sudo@kamakuraheart.org, watabe@win-int.co.jp';	
			mb_language('uni');
			mb_internal_encoding('utf-8');

			mb_send_mail($sendto, $subject, $body, $headers); 

	}
		public function sendByDrId($dr_id)
		{
			//$dr_idより、氏名等の情報を取得する。
		 	try
			{
				global $db_host;
				global $db_name;
				global $db_user;
				global $db_password;

				// MySQLサーバへ接続

				$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
				$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

				// 注文情報のorder_id取得
				$sqlStr="SELECT dr_name, email,hp_name,job_kind ";
				$sqlStr=$sqlStr."FROM dr_tbl ";
				$sqlStr=$sqlStr."where  ";
				$sqlStr=$sqlStr."id=:dr_id";
				$stmt =$pdo->prepare($sqlStr);

				$stmt->bindValue(":dr_id", $dr_id);
				$stmt->execute();

				$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

				if ($stmt->rowCount()==0) 
				{
					throw new Exception("Dr_id「".$dr_id."」が不正です。");
				}


				foreach($rows as $item)
				{
					$dr_name=$item['dr_name'];
					$email=$item['email'];
					$hp_name=$item['hp_name'];
					$job_kind=$item['job_kind'];
				}


				$this->send($dr_name,$email,$hp_name,$job_kind);


				$error="";
				$error=error_get_last();


				if($error!="")
				{
					$this->send2admin($dr_name,$email,$hp_name,$job_kind);
				}


				$pdo=null;
			}
			 catch(PDOException $e)
			{
				throw $e;
			}

		} 

		public function send($dr_name,$email,$hp_name,$job_kind) 
		{
			global $support_mail;
			global $site_name;
			global $support_mail;
			global $sender;
			global $job_kinds;
			global $site_url;



			mb_language("Japanese");
			mb_internal_encoding("UTF-8");
			mb_http_output('UTF-8');

			$subject = "${site_name} 登録完了通知メール";
			$sender = mb_encode_mimeheader("NPO TRI International Network");
			
	//　ここからメール内容
	$body = <<< _EOT_
Dear Mr/Ms/Dr {$dr_name}:
  Email: {$email}
  Affiliation: {$hp_name};
  Speciality: {$job_kinds[$job_kind]};
	
Thank you for your preregistration and payment for 
KAMAKURA LIVE DEMONSTRATION COURSE 2014.

この度は 鎌倉ライブデモンストレーション2014へ事前登録
および払込ありがとうございます。
このメールを印刷ないし、キャプチャした画面を当日　写真ID (運転免許証、
パスポート、社員証)などの本人確認可能な証明書と共に持参して下さい。
本人確認できない場合には、本人確認がなされるまで、入場証は入手できない場合が
あります。

if you have any questions, could you contact to　{$support_mail}?

ご質問やご意見ありますれば　{$support_mail} まで
お問い合わせ下さい。
===========================================================
	$site_name
	$site_url
_EOT_;
	// _EOT_　は行の最初に記述し、;の後にコメントも無し、にしないとエラーとなる	
		
		$sendto = $email;
		$headers  = "FROM: ".$sender."<$support_mail>\r\n";		
		$headers .= "Bcc:transradial@kamakuraheart.org, rie_iwamoto@kamakuraheart.org, kaori_sudo@kamakuraheart.org, watabe@win-int.co.jp";


			mb_language('uni');
			mb_internal_encoding('utf-8');

			mb_send_mail($sendto, $subject, $body, $headers); 
		}


	}
?>
