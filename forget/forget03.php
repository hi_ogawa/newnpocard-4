﻿<?php		// member_registration03.php

	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');	
	charSetUTF8();
//	session_set_cookie_params(0, "/", "/member/", TRUE, TRUE);			
	session_start();
	$today_year = date("Y");

	if (!isset($_SESSION['forget_flag'])||($_SESSION['forget_flag'] == FALSE)) {
		echo "<body bgcolor='black'>";
		echo "<h1 align='center'><font color='red'><br/><br/>Illegal Access Denied!</font></h1>";
		echo "</body>";
		session_destroy();
		exit();
	}
//接続
 	try {
    // MySQLサーバへ接続
   	$pdo = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_user, $db_password);
	// 注意: 不要なspaceを挿入すると' $db_host'のようにみなされ、エラーとなる
	} catch(PDOException $e){
    	die($e->getMessage());
	}

/*********************************************************
	2014/09/08変更（はじめ）
*********************************************************/

	$hint = hash('sha512',$_POST['hint'].$magic_code);
/*********************************************************
	2014/09/08変更（終わり）
*********************************************************/

	$stmt = $pdo->prepare("SELECT * FROM `dr_tbl` WHERE `email` = :email AND `clue` = :clue AND `hint` = :hint;");
	$stmt->bindValue(":email", $_POST['email']);
	$stmt->bindValue(":clue", $_POST['clue']);

	$stmt->bindValue(":hint", $hint);
	$stmt->execute();


	$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
	if ($stmt->rowCount() == 1) {	// このメルアド、ヒント項目、ヒント内容が既に登録されている
		
		$subject = "${site_name} 登録確認メール: email confirmation";
		$sender = mb_encode_mimeheader("NPO TRI International Network");
		$headers  = "FROM: ".$sender."<$support_mail>\r\n";		
		$parameters = '-f'.$support_mail;
		$mod_time = date('Y-m-d H:i:s');
		$md5 = substr(hash("sha512", $_POST['email'].$magic_code.$mod_time), 10, 32);
		$stmt1 = $pdo->prepare("UPDATE `dr_tbl` SET `modified` = :mod_time WHERE `id` = :dr_tbl_id;");
		$stmt1->bindValue(":mod_time", $mod_time);	// 変更リクエスト時刻を記録する
		$stmt1->bindValue(":dr_tbl_id", $row['id']);
		$stmt1->execute();

		$hint = substr(hash("sha512", $_POST['hint'].$magic_code), 10, 32);
	
	//　ここからメール内容
	$body = <<< _EOT_
電子メール・アドレス{$_POST['email']} 様
Dear Sir/Madam email address of {$_POST['email']},
	
あなたが 特定非営利活動法人ティー・アール・アイ国際ネットワークに登録された
メール・アドレス({$_POST['email']})のパスワードが変更されようとしています
あなたが確かに変更しようとしているならば　下記のリンクをクリックして下さい
もしも、間違いであれば、無視して下さい
The password for email of {$_POST['email']} is going to be changed, which
was registered to NPO TRI International Network.
If it is really to be changed, could you click the following link?
If it is not, please discard this email.
	
{$site_url}forget/forget04.php?email={$_POST['email']}&md5={$md5}&hint={$hint}

	
ご質問やご意見ありますれば　{$support_mail} まで
お問い合わせ下さい。
If you have any inquery, could you contact to {$support_mail} ?
===========================================================
	$site_name
	$site_url
_EOT_;
	// _EOT_　は行の最初に記述し、;の後にコメントも無し、にしないとエラーとなる	
		if ($_SERVER['SERVER_NAME'] === 'localhost') {
			echo "<br>".$body."<br>";
		} else {
			mb_language('uni');
			mb_internal_encoding('utf-8');
			mb_send_mail($_POST['email'], $subject, $body, $headers, "-f$support_mail"); 
		}
		$_SESSION = array();
		session_destroy();
	} else {
		header('Location: forget01.php');
		$_SESSION = array();
		session_destroy();
		exit();
	}
?>

<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="../mem_reg/member_reg_mail.css">
	<title>特定非営利活動法人ティー・アール・アイ国際ネットワークパスワード変更画面 [3/4]</title>
</head>
<body>
<h1><br/>パスワード変更画面 [3/4]<br/></h1>
<div align="center">
<table width="80%">
<tr><td class="r">
あなたの電子メール・アドレス宛てにメールを送信しました<br />
その電子メールの中にあるリンクをクリックして下さい。それによって、パスワード変更画面に進めます<br>
パスワード変更は今から10分間以内のみ有効です。</td></tr>
<tr><td class="b">** 1時間以内にメールが届かない場合、あなたの入力されたメール・アドレスが間違っている可能性があります。</td></tr>
<tr><td class="g">あるいは、送付しました 電子メールが迷惑メールとして扱われ、自動的に「迷惑メール・フォルダ」に
振り分けられている可能性もあります。従いまして、 「迷惑メール・フォルダ」もチェックして頂ければ幸いです</td></tr>
<tr><td class="r">
The system sent an email to you.<br />
You can click the link within the email, and then you can proceed to the password change process.<br>
Passowrd change process can work before 10 minutes pass after now on.</td></tr>
<tr><td class="b">** If you do not receive an email, your email address may be wrong.</td></tr>
<tr><td class="g">Or, the email might be transferred to your annoyance folder. Thus, could you check the folder as well?</td></tr>
</table>
</div>
</bodY>
</html>
