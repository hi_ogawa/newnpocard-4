<?php		// member_registration01.php

	require_once('../utilities/config.php');
	require_once('../utilities/lib.php');	
	charSetUTF8();
	session_set_cookie_params(0, "/", "/member/", TRUE, TRUE);
	session_start();
	$today_year = date("Y");
	$_SESSION['forget_flag'] = TRUE;

?>


<!DOCTYPE html>
<html lang="jp">
<head>
<meta charset="utf-8">
<meta name="description" content="NPO TRI International Network">
<meta name="author" content="Shigeru SAITO, MD, FACC, FSCAI, FJCC">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="Thu, 01 Dec 1994 16:00:00 GMT">
<link rel="stylesheet" type="text/css" href="../css/index.css"/>
 <script src="../javascript/jquery-1.10.2.js"></script>
<script src="../javascript/jquery-corner.js"></script>
<script src="../javascript/index.js"></script>
<title>NPO TRI</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="forget.css">
</head>
<body>
<div id="main">
<h1>Reset Password<br/><br/></h1>

<table>
<form action="forget02.php" method="post">
	
	<tr><td>あなたの電子メール・アドレス : </td>
	<td><input id="mail" type="text" name="email" size=64 maxlength=128></td></tr>
	<br />
	<tr><td>パスワード忘れた時のために選んだ項目は何でしたか? : </td>
    <td><select name="clue">
    <?php
		foreach($hints as $clue => $hint_item) {
    		echo "<option value=$clue>$hint_item</option>";
		}
	?>
        </select>
   	</td></tr>
	<tr><td>あなたの入力した回答は? </td><td><input type="text" name="hint" size="30" maxlength="30" class="in" /></td></tr>    

<tr><td colspan="2" align="center"><input type="submit" value="- 確認 -"></td></tr>
</form>
</div>
</body>
</html>